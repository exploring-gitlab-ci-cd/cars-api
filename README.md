## Java
How to build a java artifact?

```
$ vagrant up
$ vagrant ssh
$ ./gradlew build  # (run $ ./gradlew clean - to delete build/directory)
```

You will find a artifact inside **build/lib/** if build succeeds.
## How to use AWS secrets in GitLab CI?
Go to group|project -> settings -> CI/CD -> variables (unmark protected)

But make sure that you've created new user with only nesseary permissions.

You should specify following variables:
```
AWS_DEFAULT_REGION
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
```