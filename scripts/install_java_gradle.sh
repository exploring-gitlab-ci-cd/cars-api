#!/bin/bash
sudo apt update -y
sudo apt install -y openjdk-11-jdk

VERSION=6.5.1
wget https://services.gradle.org/distributions/gradle-${VERSION}-bin.zip -P /tmp
sudo apt install -y unzip
sudo unzip -d /opt/gradle /tmp/gradle-${VERSION}-bin.zip
sudo ln -s /opt/gradle/gradle-${VERSION} /opt/gradle/latest

ZSH_PATH=/home/vagrant/.zshrc

if test -f $ZSH_PATH; then 
    PATH_TO_ENV=$ZSH_PATH
else
    PATH_TO_ENV=/etc/profile.d/gradle.sh
    sudo touch $PATH_TO_ENV && sudo chmod +x $PATH_TO_ENV
fi

cat << EOF >> $PATH_TO_ENV
export GRADLE_HOME=/opt/gradle/latest
export PATH=\${GRADLE_HOME}/bin:\${PATH}
EOF
