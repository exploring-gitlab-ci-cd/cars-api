output "vpc_id" {
  value       = aws_elastic_beanstalk_environment.cars-api.cname
  description = "java app endpoint"
}